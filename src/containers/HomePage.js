import { connect } from 'react-redux'
import {
  selectIsChanging,
  selectIsLoaded,
  selectTokenizeCurrentIndex,
  selectFileSize,
  selectExports,
} from 'store/parser/selectors'
import { selectRealmsLoaded } from 'store/realms/selectors'
import { selectRealm } from 'store/user/selectors'
import HomePage from 'components/pages/HomePage'

const mapStateToProps = state => ({
  accountingFileIsChanging: selectIsChanging(state),
  accountingFileIsLoaded: selectIsLoaded(state),
  currentTokenizingIndex: selectTokenizeCurrentIndex(state),
  fileSize: selectFileSize(state),
  accountingFileExports: selectExports(state),
  realmsLoaded: selectRealmsLoaded(state),
  selectedRealm: selectRealm(state),
})

export default connect(mapStateToProps)(HomePage)
