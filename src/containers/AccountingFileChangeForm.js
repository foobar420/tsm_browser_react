import React from 'react' // eslint-disable-line no-unused-vars
import { connect } from 'react-redux'
import { compose } from 'recompose'
import { reduxForm } from 'redux-form'

import { AccountingFileChangeForm } from 'components'
import { createValidator, fileType, required } from 'services/validation'
import { parserChangeFile } from 'store/parser/actions'
import {
  selectIsChanging,
  selectIsReading,
  selectIsTokenizing,
  selectIsParsing,
  selectIsLoaded,
  selectFileSize,
  selectTokenizeCurrentIndex,
  selectTokenizeMaxIndex,
} from 'store/parser/selectors'

const mapStateToProps = state => ({
  parserIsChanging: selectIsChanging(state),
  parserIsReading: selectIsReading(state),
  parserIsTokenizing: selectIsTokenizing(state),
  parserIsParsing: selectIsParsing(state),
  parserIsLoaded: selectIsLoaded(state),
  parserTokenizeCurrentIndex: selectTokenizeCurrentIndex(state),
  parserTokenizeMaxIndex: selectTokenizeMaxIndex(state),
  fileSize: selectFileSize(state),
})

const mapDispatchToProps = {
  parserChangeFile,
}

const onChange = ({ file }, dispatch) =>
  new Promise((resolve, reject) => dispatch(parserChangeFile({ file }, resolve, reject)))

const validate = createValidator({
  file: [required, fileType('pdf')],
})

const reduxFormOpts = {
  form: 'AccountingFileChangeForm',
  validate,
  asyncValidate: validate,
  asyncChangeFields: ['file'],
  onChange,
}

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm(reduxFormOpts)
)

export default enhance(AccountingFileChangeForm)
