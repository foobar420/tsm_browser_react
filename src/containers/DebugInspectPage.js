import { connect } from 'react-redux'
import { selectIsLoaded } from 'store/parser/selectors'
import { selectTSMDB } from 'store/accountingFile/selectors'
import DebugInspectPage from 'components/pages/DebugInspectPage'

const mapStateToProps = state => ({
  accountingFileIsLoaded: selectIsLoaded(state),
  tsmDB: selectTSMDB(state),
})

export default connect(mapStateToProps)(DebugInspectPage)
