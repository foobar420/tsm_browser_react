import { compose } from 'recompose'
import { connect } from 'react-redux'

import { selectSales } from 'store/sales/selectors'
import { selectBuys } from 'store/buys/selectors'
import { selectRealm } from 'store/user/selectors'

import OverviewChart from 'components/organisms/OverviewChart'

const mapStateToProps = (state) => {
  const realm = selectRealm(state)
  return {
    buys: selectBuys(state, realm),
    realm,
    sales: selectSales(state, realm),
  }
}

const enhance = compose(connect(mapStateToProps))

export default enhance(OverviewChart)
