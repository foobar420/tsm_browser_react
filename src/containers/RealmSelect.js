import { compose } from 'recompose'
import { connect } from 'react-redux'
import { selectRealms } from 'store/realms/selectors'
import { changeRealm } from 'store/user/actions'
import { selectRealm } from 'store/user/selectors'
import RealmSelect from 'components/organisms/RealmSelect'

const mapStateToProps = state => ({
  selectedRealm: selectRealm(state),
  realms: selectRealms(state),
})

const mapDispatchToProps = { changeRealm }

const enhance = compose(connect(mapStateToProps, mapDispatchToProps))

export default enhance(RealmSelect)
