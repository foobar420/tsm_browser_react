import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import { withStyles } from '@material-ui/core/styles'
import ErrorIcon from '@material-ui/icons/Error'


const styles = theme => ({
  snackbar: {
    margin: theme.spacing.unit,
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
})

class ChartErrorBoundary extends React.Component {

  state = {
    error: null,
  }

  componentDidCatch(error) {
    this.setState({ error })
  }

  renderError() {
    const { classes } = this.props
    return (
      <SnackbarContent
        className={classNames(classes.snackbar, classes.error)}
        message={
          <span>
            <ErrorIcon className={classNames(classes.icon, classes.iconVariant)} />
            An error occured loading the chart
          </span>
        }
      />
    )
  }

  render() {
    if (this.state.error) return this.renderError()
    return (
      <React.Fragment>{this.props.children}</React.Fragment>
    )
  }
}

ChartErrorBoundary.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}

export default withStyles(styles, { withTheme: true })(ChartErrorBoundary)

