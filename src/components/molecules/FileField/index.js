import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ifProp } from 'styled-tools'
import Button from '@material-ui/core/Button'

const Wrapper = styled.div`
  display: flex;
`

const Input = styled.input`
  width: 0.1px;
  height: 0.1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
`

const Label = styled.label`
  display: ${ifProp('hidden', 'none', 'initial')};
`

const FileField = ({
  hidden,
  title,
  labelProps,
  ...props
}) => (
  <Wrapper>
    <Button color="secondary" htmlFor="file" style={{ display: hidden ? 'none' : 'initial' }} component={Label}>
      {title}
    </Button>
    <Input type="file" {...props} />
  </Wrapper>
)

FileField.propTypes = {
  hidden: PropTypes.bool,
  title: PropTypes.string.isRequired,
  labelProps: PropTypes.shape({
    disabled: PropTypes.bool,
    palette: PropTypes.string,
    transparent: PropTypes.bool,
    reverse: PropTypes.bool,
    height: PropTypes.number,
    type: PropTypes.string,
  }),
}

FileField.defaultProps = {
  hidden: false,
  labelProps: {
    palette: 'secondary',
    type: 'button',
    height: 40,
  },
}

export default FileField
