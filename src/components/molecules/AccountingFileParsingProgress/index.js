import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import CircularProgress from '@material-ui/core/CircularProgress'
import Typography from '@material-ui/core/Typography'

const StyledMolecule = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

const getDisplaySize = (size) => {
  let unit = 'b'
  let displaySize = size
  if (size > 1024 * 1024 * 1024) {
    unit = 'gb'
    displaySize /= 1024 * 1024 * 1024
  } else if (size > 1024 * 1024) {
    unit = 'mb'
    displaySize /= 1024 * 1024
  } else if (size > 1024) {
    unit = 'kb'
    displaySize /= 1024
  }
  return {
    displaySize: Math.round(displaySize),
    unit,
  }
}

const AccountingFileParsingProgress = ({
  parserIsChanging,
  parserTokenizeCurrentIndex,
  fileSize,
}) => {
  const fileDisplaySize = getDisplaySize(fileSize)
  const parsedSize = getDisplaySize(parserTokenizeCurrentIndex)
  const pct = parserTokenizeCurrentIndex ? Math.round((parserTokenizeCurrentIndex / fileSize) * 100) : 0
  return (
    <StyledMolecule>
      {parserIsChanging && <CircularProgress value={pct} variant="static" color="secondary" style={{ marginBottom: '1rem' }} /> }
      {parserIsChanging && <Typography>Please wait while the file is being processed</Typography>}
      {parserIsChanging && <Typography>{parsedSize.displaySize}{parsedSize.unit} of {fileDisplaySize.displaySize}{fileDisplaySize.unit} ({pct}%)</Typography>}
    </StyledMolecule>
  )
}

AccountingFileParsingProgress.propTypes = {
  parserIsChanging: PropTypes.bool.isRequired,
  parserTokenizeCurrentIndex: PropTypes.number.isRequired,
  fileSize: PropTypes.number,
}

export default AccountingFileParsingProgress
