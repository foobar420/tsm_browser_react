// https://github.com/diegohaz/arc/wiki/Atomic-Design
import React from 'react'
import PropTypes from 'prop-types'


import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import IconButton from '@material-ui/core/IconButton'
import Snackbar from '@material-ui/core/Snackbar'

import CloseIcon from '@material-ui/icons/Close'

import { BasicTemplate, IntroductionDialog } from 'components'
import { OverviewChart } from 'containers'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  card: {
    display: 'flex',
    minWidth: 400,
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
})

class HomePage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      introductionDialogOpen: !props.accountingFileIsLoaded,
      showAccountingFileLoadedSnackbar: false,
    }
  }

  handleDialogClose = () => this.setState({
    introductionDialogOpen: false,
    showAccountingFileLoadedSnackbar: true,
  })

  handleAccountingFileLoadedSnackbarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }
    this.setState({
      showAccountingFileLoadedSnackbar: false,
    })
  }

  renderContent = () => {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <div className={classes.details}>
            <CardContent className={classes.content}>
              <OverviewChart />
            </CardContent>
          </div>
        </Card>
      </div>
    )
  }

  render = () => {
    const {
      accountingFileIsChanging,
      accountingFileIsLoaded,
      classes,
      currentTokenizingIndex,
      realmsLoaded,
      selectedRealm,
      fileSize,
    } = this.props
    return (
      <BasicTemplate>
        {accountingFileIsLoaded && selectedRealm.length && this.renderContent()}
        <IntroductionDialog
          accountingFileIsChanging={accountingFileIsChanging}
          currentTokenizingIndex={currentTokenizingIndex}
          fileSize={fileSize}
          open={this.state.introductionDialogOpen}
          handleClose={this.handleDialogClose}
          realmsLoaded={realmsLoaded}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.showAccountingFileLoadedSnackbar}
          autoHideDuration={2500}
          onClose={this.handleAccountingFileLoadedSnackbarClose}
          message={<span id="message-id">{accountingFileIsLoaded ? 'Accounting File loaded successfully' : 'Could not load accounting file'}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleAccountingFileLoadedSnackbarClose}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
      </BasicTemplate>
    )
  }
}

HomePage.propTypes = {
  accountingFileIsChanging: PropTypes.bool.isRequired,
  accountingFileIsLoaded: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
  currentTokenizingIndex: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  fileSize: PropTypes.number,
  realmsLoaded: PropTypes.bool.isRequired,
  selectedRealm: PropTypes.string.isRequired,
}

export default withStyles(styles, { withTheme: true })(HomePage)
