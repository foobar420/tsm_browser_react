// https://github.com/diegohaz/arc/wiki/Atomic-Design
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'

import IconToggleOn from 'my_icons/ToggleOn'
import IconToggleOff from 'my_icons/ToggleOff'

import { BasicTemplate } from 'components'

const StyledPre = styled.pre`
  white-space: pre-wrap;
`

const StyledSpan = styled.span`
  font-family: Roboto;
  font-size: 16px;
`

class AccountingFileExportEntry extends React.Component {
  state = {
    expanded: false,
  }
  expand = () => this.setState({ expanded: true })
  collapse = () => this.setState({ expanded: false })
  toggle = () => this.setState({ expanded: !this.state.expanded })

  render() {
    const { expanded } = this.state
    return (
      <div>
        <IconButton onClick={this.toggle}>
          {expanded && <IconToggleOn />}
          {!expanded && <IconToggleOff />}
        </IconButton>
        <StyledSpan>{this.props.accountingFileKey}</StyledSpan>
        {expanded && <StyledPre>{JSON.stringify(this.props.accountingFileValue)}</StyledPre>}
      </div>
    )
  }
}

AccountingFileExportEntry.propTypes = {
  accountingFileKey: PropTypes.string.isRequired,
  accountingFileValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
    PropTypes.array,
    PropTypes.object,
  ]).isRequired,
}

const AccountingFileExports = ({ tsmDB }) => (
  <div>
    {
      Object.keys(tsmDB)
        .sort()
        .map(key => (
          <AccountingFileExportEntry
            key={key}
            accountingFileKey={key}
            accountingFileValue={tsmDB[key]}
          />
        ))
    }
  </div>
)

AccountingFileExports.propTypes = {
  tsmDB: PropTypes.object.isRequired,
}


class DebugInspectPage extends React.Component {

  componentDidMount() {
    if (!this.props.accountingFileIsLoaded) {
      this.props.history.push('/')
    }
  }

  render() {
    const { accountingFileIsLoaded, tsmDB } = this.props
    return (
      <BasicTemplate>
        {accountingFileIsLoaded && <AccountingFileExports tsmDB={tsmDB} />}
      </BasicTemplate>
    )
  }
}

DebugInspectPage.propTypes = {
  accountingFileIsLoaded: PropTypes.bool.isRequired,
  history: PropTypes.object,
  tsmDB: PropTypes.object,
}

export default DebugInspectPage
