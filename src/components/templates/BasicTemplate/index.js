import React from 'react'
import PropTypes from 'prop-types'

import AppBar from '@material-ui/core/AppBar'
import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'

import DescriptionIcon from '@material-ui/icons/Description'
import HomeIcon from '@material-ui/icons/Home'
import MenuIcon from '@material-ui/icons/Menu'

import withStyles from '@material-ui/core/styles/withStyles'

import { RealmSelect } from 'containers'


const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  flexGrow: {
    flexGrow: 1,
  },
  flex: {
    flex: 1,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    minWidth: 0, // So the Typography noWrap works
  },
  drawerButton: {
    marginTop: theme.spacing.unit,
    marginLeft: theme.spacing.unit * 3,
  },
  list: {
    position: 'relative',
    width: 240,
  },
  toolbarMixin: theme.mixins.toolbar,
  toolbar: {
    ...theme.mixins.toolbar,
    flexGrow: 1,
  },
  realmSelectWrapper: {
    marginTop: -theme.spacing.unit * 2, // a bit dirty
  },
})


class BasicTemplate extends React.Component {

  state = { drawerOpen: false }

  render() {
    const { children, classes } = this.props
    return (
      <div className={classes.root}>
        <AppBar position="absolute" className={classes.appBar}>
          <Toolbar className={classes.toolbar}>
            <Typography variant="title" color="inherit" noWrap className={classes.flex}>
              TSM Accountant
            </Typography>
            <div className={classes.realmSelectWrapper}>
              <RealmSelect />
            </div>
          </Toolbar>
        </AppBar>
        <Drawer
          anchor="left"
          open={this.state.drawerOpen}
          variant="permanent"
          onClose={() => this.setState({ drawerOpen: false })}
        >
          <IconButton className={classes.drawerButton} onClick={() => this.setState({ drawerOpen: false })}>
            <MenuIcon />
          </IconButton>
          <List className={classes.list}>
            <ListItem button to="/">
              <ListItemIcon>
                <HomeIcon />
              </ListItemIcon>
              <ListItemText primary="Home" />
            </ListItem>
            <ListItem button to="/debug-inspect">
              <ListItemIcon>
                <DescriptionIcon />
              </ListItemIcon>
              <ListItemText primary="Debug Inspect" />
            </ListItem>
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbarMixin} />
          {children}
        </main>
      </div>
    )
  }
}

BasicTemplate.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  classes: PropTypes.object,
}

export default withStyles(styles, { withTheme: true })(BasicTemplate)
