import React from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
})

class RealmSelect extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      value: props.selectedRealm,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedRealm !== this.state.value) {
      this.changeValue(nextProps.selectedRealm)
    }
  }

  handleChange = (ev) => {
    const { value } = ev.target
    if (value.length) {
      this.props.changeRealm(value)
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(value)
      }
    }
    this.changeValue(value)
  }

  changeValue = (value) => {
    this.setState({ value })
  }

  render() {
    const {
      classes,
      realms,
      showEmptyOption,
    } = this.props
    return (
      <form className={classes.root} autoComplete="off">
        <FormControl className={classes.formControl}>
          {!this.state.value && <InputLabel htmlFor="realm">Realm</InputLabel>}
          <Select
            value={this.state.value}
            onChange={this.handleChange}
            className={classes.selectEmpty}
            inputProps={{
              name: 'realm',
              id: 'realm',
              value: this.state.value,
            }}
          >
            {showEmptyOption && <MenuItem value="">Select Realm...</MenuItem>}
            {realms.map(realm => <MenuItem key={realm} value={realm}>{realm}</MenuItem>)}
          </Select>
        </FormControl>
      </form>
    )
  }
}

RealmSelect.propTypes = {
  onChange: PropTypes.func,
  changeRealm: PropTypes.func.isRequired,
  selectedRealm: PropTypes.string,
  classes: PropTypes.object.isRequired,
  realms: PropTypes.array.isRequired,
  showEmptyOption: PropTypes.bool,
}

RealmSelect.defaultProps = {
  showEmptyOption: false,
}

export default withStyles(styles, { withTheme: true })(RealmSelect)
