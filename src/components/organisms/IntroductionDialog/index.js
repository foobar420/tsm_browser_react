import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import CircularProgress from '@material-ui/core/CircularProgress'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'

import { AccountingFileParsingProgress } from 'components'
import { AccountingFileChangeForm, RealmSelect } from 'containers'

const StyledAccountingFileChangeForm = styled(AccountingFileChangeForm)`
  display: ${props => props.hidden ? 'none' : 'initial'};
`

class IntroductionDialog extends React.Component {

  state = {
    accountingFileChanged: false,
  }

  handleAccountingFileChanged = () => {
    this.setState({ accountingFileChanged: true })
  }

  handleRealmSelected = () => this.props.handleClose()

  renderAccountingFileChangeForm() {
    const {
      accountingFileIsChanging,
      currentTokenizingIndex,
      fileSize,
    } = this.props
    return (
      <React.Fragment>
        <DialogContent>
          <DialogContentText>
            To get started, please select the TradeSkillMaster.lua file in the
            WTF/Account/Your Account/SavedVariables directory of your World of Warcraft installation.
          </DialogContentText>
          <AccountingFileParsingProgress
            parserIsChanging={accountingFileIsChanging}
            parserTokenizeCurrentIndex={currentTokenizingIndex}
            fileSize={fileSize}
          />
        </DialogContent>
        <DialogActions>
          <StyledAccountingFileChangeForm
            onFinish={this.handleAccountingFileChanged}
            hidden={accountingFileIsChanging}
          />
        </DialogActions>
      </React.Fragment>
    )
  }

  renderRealmSelect() {
    return (
      <React.Fragment>
        <DialogContent>
          <DialogContentText>
            Please select a realm to begin
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <RealmSelect onChange={this.handleRealmSelected} />
        </DialogActions>
      </React.Fragment>
    )
  }

  renderIntermediateLoader() {
    return (
      <DialogContent>
        <CircularProgress variant="indeterminate" style={{ marginBottom: '1rem' }} />
      </DialogContent>
    )
  }

  render() {
    return (
      <Dialog open={this.props.open}>
        <DialogTitle>Welcome</DialogTitle>
        {!this.state.accountingFileChanged && this.renderAccountingFileChangeForm()}
        {this.props.realmsLoaded && this.renderRealmSelect()}
        {this.state.accountingFileChanged && !this.props.realmsLoaded && this.renderIntermediateLoader()}
      </Dialog>
    )
  }
}

IntroductionDialog.propTypes = {
  accountingFileIsChanging: PropTypes.bool.isRequired,
  currentTokenizingIndex: PropTypes.number,
  fileSize: PropTypes.number,
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  realmsLoaded: PropTypes.bool.isRequired,
}

export default IntroductionDialog
