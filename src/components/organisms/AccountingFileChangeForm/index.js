import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { FileField } from 'components'

const StyledFileField = styled(FileField)`
  ${props => props.hidden ? 'display: none;' : ''}
`

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
`

class AccountingFileChangeForm extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (this.props.parserIsChanging && !nextProps.parserIsChanging) {
      this.props.onFinish && this.props.onFinish()
    }
  }

  onFileChange = (ev) => {
    ev.preventDefault()
    this.props.change('file', ev.target.files[0])
  }

  render() {
    const {
      className,
      parserIsChanging,
      parserIsLoaded,
    } = this.props
    return (
      <StyledForm name="AccountingFileChangeForm" className={className || ''}>
        {!parserIsLoaded && <StyledFileField
          name="file"
          type="file"
          id="file"
          onChange={this.onFileChange}
          hidden={parserIsChanging}
          labelProps={{
            palette: 'primary',
            height: 40,
          }}
          title="Select..."
        />}
      </StyledForm>
    )
  }
}

AccountingFileChangeForm.propTypes = {
  change: PropTypes.func.isRequired,
  className: PropTypes.string,
  fileSize: PropTypes.number,
  parserIsChanging: PropTypes.bool.isRequired,
  parserIsLoaded: PropTypes.bool.isRequired,
  onFinish: PropTypes.func,
}

export default AccountingFileChangeForm
