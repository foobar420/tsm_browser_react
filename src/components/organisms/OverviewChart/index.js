import React from 'react'
import PropTypes from 'prop-types'

import moment from 'moment'
import lodash from 'lodash'

import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { withStyles } from '@material-ui/core/styles'

import { curveCatmullRom } from 'd3-shape'

import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  VerticalGridLines,
  LineSeries,
} from 'react-vis'

import ChartErrorBoundary from 'components/molecules/ChartErrorBoundary'

const MILLISECONDS_PER_DAY = 1000 * 60 * 60 * 24

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  listItem: {
    background: theme.palette.primary.dark,
    padding: theme.spacing.unit,
    ...theme.typography.body1,
  }
});


class OverviewChart extends React.Component {

  constructor(props) {
    super(props)
    const endDate = moment().format('YYYY-MM-DD')
    const startDate = moment().subtract(1, 'month').format('YYYY-MM-DD')
    this.state = {
      startDate,
      endDate,
      buysByDay: this.prepareSeriesData(props.buys, startDate, endDate),
      salesByDay: this.prepareSeriesData(props.sales, startDate, endDate),
      dailyDetails: [],
    }
  }

  componentWillReceiveProps(nextProps) {
    const { endDate, startDate } = this.state
    if (nextProps.buys.hash !== this.props.buys.hash) {
      this.setState({ buysByDay: this.prepareSeriesData(nextProps.buys, startDate, endDate) })
    }
    if (nextProps.sales.hash !== this.props.sales.hash) {
      this.setState({ salesByDay: this.prepareSeriesData(nextProps.sales, startDate, endDate) })
    }
  }

  refreshSeriesData = (startDate, endDate) => {
    this.setState({ buysByDay: this.prepareSeriesData(this.props.buys, startDate, endDate) })
    this.setState({ salesByDay: this.prepareSeriesData(this.props.sales, startDate, endDate) })
  }

  handleStartDateChange = (ev) => {
    const startDate = moment(ev.target.value).format('YYYY-MM-DD')
    this.setState({ startDate })
    this.refreshSeriesData(startDate, this.state.endDate)
  }

  handleEndDateChange = (ev) => {
    const endDate = moment(ev.target.value).format('YYYY-MM-DD')
    this.setState({ endDate })
    this.refreshSeriesData(this.state.startDate, endDate)
  }

  handleChartNearestX = (datapoint) => {
    const day = parseInt(datapoint.x, 10)
    const buys = this.state.buysByDay[day] || []
    const sales = this.state.salesByDay[day] || []
    const dailyDetails = [].concat(buys.map(entry => ({ ...entry, type: 'buy' }))).concat(sales.map(entry => ({ ...entry, type: 'sale' })))
    this.setState({
      dailyDetails,
    })
    if (window.$WowheadPower && window.$WowheadPower.refreshLinks) {
      window.$WowheadPower.refreshLinks()
    }
    console.log("Nearest X!", {
      dailyDetails,
      day,
    })
  }

  prepareSeriesData = (entries, startDate, endDate) => lodash.groupBy(entries.filter(entry => moment(entry.time).isBetween(moment(startDate), moment(endDate))), entry => Math.floor(entry.time / MILLISECONDS_PER_DAY))

  mapToSeriesData = entriesByDay => Object.keys(entriesByDay).map(
    day => ({
      x: day,
      y: lodash.sumBy(entriesByDay[day], entry => entry.price * entry.quantity * entry.stackSize)
    })
  )

  renderSalesForm() {
    const {
      endDate,
      startDate,
    } = this.state
    const { classes } = this.props
    return (
      <form className={classes.container} noValidate>
        <TextField
          id="startDate"
          label="Start Date"
          type="date"
          value={startDate}
          onChange={this.handleStartDateChange}
          className={classes.textField}
          InputLabelProps={{ shrink: true }}
        />
        <TextField
          id="endDate"
          label="End Date"
          type="date"
          value={endDate}
          onChange={this.handleEndDateChange}
          className={classes.textField}
          InputLabelProps={{ shrink: true }}
        />
      </form>
    )
  }

  renderDailyDetails() {
    const { dailyDetails } = this.state
    const { classes } = this.props
    return (
      <List dense>
        {dailyDetails.map((entry) => {
          const itemId = entry.itemString.split(':')[1]
          return (
            <ListItem className={classes.listItem} color="primary">
              <ListItemText
                primary={<a href="#" data-wh-rename-link="true" data-wowhead={`item=${itemId}`}>Loading...</a>}
                secondary={<span>{entry.quantity} * ({entry.price} [{entry.stackSize}]){moment(entry.time).format('LLL')}</span>}
              />
            </ListItem>
          )
        })}
      </List>
    )
  }

  render() {
    const { dailyDetails } = this.state
    const buys = this.mapToSeriesData(this.state.buysByDay)
    const sales = this.mapToSeriesData(this.state.salesByDay)
    return (
      <React.Fragment>
        <Typography variant="headline">Sales vs Buys</Typography>
        {this.renderSalesForm()}
        <ChartErrorBoundary>
          {(sales.length || buys.length) &&
            <div>
              <XYPlot
                width={640}
                height={480}
              >
                <HorizontalGridLines />
                <VerticalGridLines />
                <XAxis tickFormat={val => moment(val * MILLISECONDS_PER_DAY).format('MM-DD')} position="start" />
                <YAxis tickFormat={val => val / 10000} />
                <LineSeries
                  className="buys-series"
                  data={buys}
                  stroke="red"
                  onNearestX={this.handleChartNearestX}
                />
                <LineSeries
                  className="sales-series"
                  data={sales}
                  stroke="green"
                  onNearestX={this.handleChartNearestX}
                />
              </XYPlot>
              {!!dailyDetails.length && this.renderDailyDetails()}
            </div>
          }
        </ChartErrorBoundary>
      </React.Fragment>
    )
  }
}

OverviewChart.propTypes = {
  buys: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  sales: PropTypes.array.isRequired,
}

export default withStyles(styles, { withTheme: true })(OverviewChart)
