import React from 'react'
import { Switch, Route } from 'react-router-dom'

import { NotFoundPage } from 'components'
import { HomePage, DebugInspectPage } from 'containers'

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

import '../../node_modules/react-vis/dist/main.scss'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#fff',
      main: '#f9f9f9',
      dark: '#c6c6c6',
      contrastText: '#000',
    },
    secondary: {
      light: '#9162e4',
      main: '#5e35b1',
      dark: '#280680',
      contrastText: '#fff',
    },
  },
})

const App = () => {
  return (
    <React.Fragment>
      <CssBaseline />
      <MuiThemeProvider theme={theme}>
        <Switch>
          <Route path="/" component={HomePage} exact />
          <Route path="/debug-inspect" component={DebugInspectPage} exact />
          <Route component={NotFoundPage} />
        </Switch>
      </MuiThemeProvider>
    </React.Fragment>
  )
}

export default App
