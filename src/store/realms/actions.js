import { createAction } from 'redux-actions'
import identity from 'lodash/identity'
import { tsmDBMetaCreator } from 'utils/actions'

export const REALMS_LOADED = 'REALMS_LOADED'
export const realmsLoaded = createAction(REALMS_LOADED, identity, tsmDBMetaCreator)
