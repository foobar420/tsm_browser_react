import { all, call, put, takeLatest } from 'redux-saga/effects'
import { ACCOUNTING_FILE_LOADED } from 'store/accountingFile/actions'
import { getRealms } from 'utils/accountingFile'
import { realmsLoaded } from './actions'

function* loadRealmsFlow(action) {
  const { tsmDB } = action.payload
  yield put(realmsLoaded(getRealms(tsmDB), tsmDB))
}

function* watchAccountingFileLoaded() {
  yield takeLatest(ACCOUNTING_FILE_LOADED, loadRealmsFlow)
}

export default function* rootSaga() {
  yield all([
    call(watchAccountingFileLoaded),
  ])
}
