import { handleActions } from 'redux-actions'
import { CHANGE_REALM, REALMS_LOADED } from './actions'

const initialState = {
  realms: [],
  loaded: false,
  selectedRealm: '',
}

export default handleActions(
  {
    [REALMS_LOADED]: {
      next(state, action) {
        return {
          ...state,
          loaded: true,
          realms: action.payload,
        }
      },
    },
    [CHANGE_REALM]: {
      next(state, action) {
        console.log('CHANGE_REALM', action.payload)
        return {
          ...state,
          selectedRealm: action.payload,
        }
      },
    },
  },
  initialState
)
