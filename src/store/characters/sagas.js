import { all, call, put, takeLatest } from 'redux-saga/effects'
import { ACCOUNTING_FILE_LOADED } from 'store/accountingFile/actions'
import { getAllCharacters, getCharacterClass } from 'utils/accountingFile'
import { charactersLoaded } from './actions'

function* loadCharactersFlow(action) {
  const { tsmDB } = action.payload
  const characters = getAllCharacters(tsmDB).map(char => ({
    ...char,
    characterClass: getCharacterClass(tsmDB, char.realm, char.faction, char.character),
  }))
  yield put(charactersLoaded(characters, tsmDB))
}

function* watchAccountingFileLoaded() {
  yield takeLatest(ACCOUNTING_FILE_LOADED, loadCharactersFlow)
}

export default function* rootSaga() {
  yield all([
    call(watchAccountingFileLoaded),
  ])
}
