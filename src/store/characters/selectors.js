export const selectAllCharacters = state => state.characters.characters
export const selectCharactersByRealm = (state, realm) => state.characters.characters.filter(char => char.realm === realm)
export const selectCharactersByRealmFaction = (state, realm, faction) => state.characters.filter(char => char.realm === realm && char.faction === faction)
