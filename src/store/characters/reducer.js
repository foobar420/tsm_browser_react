import { handleActions } from 'redux-actions'
import { CHARACTERS_LOADED } from './actions'

const initialState = {
  characters: [],
}

export default handleActions(
  {
    [CHARACTERS_LOADED]: {
      next(state, action) {
        return {
          ...state,
          characters: action.payload,
        }
      },
    },
  },
  initialState
)
