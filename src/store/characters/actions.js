import { createAction } from 'redux-actions'
import identity from 'lodash/identity'
import { tsmDBMetaCreator } from 'utils/actions'

export const CHARACTERS_LOADED = 'CHARACTERS_LOADED'
export const charactersLoaded = createAction(CHARACTERS_LOADED, identity, tsmDBMetaCreator)
