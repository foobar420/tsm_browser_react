import { all, call, put, takeLatest } from 'redux-saga/effects'
import { PARSER_PARSE_TOKENS_COMPLETE } from 'store/parser/actions'
import { accountingFileLoaded } from './actions'

function* loadAccountingFileFlow(action) {
  yield put(accountingFileLoaded({ tsmDB: action.payload.exports.TradeSkillMasterDB }))
}

function* watchLoadAccountingFile() {
  yield takeLatest(PARSER_PARSE_TOKENS_COMPLETE, loadAccountingFileFlow)
}

export default function* rootSaga() {
  yield all([
    call(watchLoadAccountingFile),
  ])
}
