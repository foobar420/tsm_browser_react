import { createAction } from 'redux-actions'

export const ACCOUNTING_FILE_LOADED = 'ACCOUNTING_FILE_LOADED'
export const accountingFileLoaded = createAction(ACCOUNTING_FILE_LOADED)
