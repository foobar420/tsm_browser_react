import { handleActions } from 'redux-actions'
import { ACCOUNTING_FILE_LOADED } from './actions'

const initialState = {
  tsmDB: {},
}

export default handleActions(
  {
    [ACCOUNTING_FILE_LOADED]: {
      next(state, action) {
        return {
          ...state,
          tsmDB: action.payload.tsmDB,
        }
      },
    },
  },
  initialState
)
