import { createAction } from 'redux-actions'
import identity from 'lodash/identity'
import { tsmDBMetaCreator } from 'utils/actions'

export const SALES_LOADED = 'SALES_LOADED'
export const salesLoaded = createAction(SALES_LOADED, identity, tsmDBMetaCreator)
