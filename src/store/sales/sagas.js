import { all, call, put, takeLatest } from 'redux-saga/effects'
// import moment from 'moment'
import { REALMS_LOADED } from 'store/realms/actions'
import { getSales } from 'utils/accountingFile'
import { salesLoaded } from './actions'

function* loadSalesFlow(action) {
  const realms = action.payload
  const sales = realms.reduce((sales, realm) => ({
    ...sales,
    [realm]: getSales(action.meta.tsmDB, realm),
  }), {})
  yield put(salesLoaded(sales, action.meta.tsmDB))
}

function* watchRealmsLoaded() {
  yield takeLatest(REALMS_LOADED, loadSalesFlow)
}

export default function* rootSaga() {
  yield all([
    call(watchRealmsLoaded),
  ])
}
