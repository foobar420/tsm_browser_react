export const selectSales = (state, realm) => {
  if (!(realm in state.sales.sales)) {
    return {}
  }
  return state.sales.sales[realm]
}
