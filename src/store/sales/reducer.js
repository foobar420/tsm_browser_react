import { handleActions } from 'redux-actions'
import { SALES_LOADED } from './actions'

const initialState = {
  sales: {},
}

const formatSale = sale => ({
  ...sale,
  time: parseInt(sale.time, 10) * 1000,
  price: parseInt(sale.price, 10),
  quantity: parseInt(sale.quantity, 10),
  stackSize: parseInt(sale.stackSize, 10),
})

export default handleActions(
  {
    [SALES_LOADED]: {
      next(state, action) {
        console.log(action.payload)
        return {
          ...state,
          sales: Object.entries(action.payload).reduce((sales, [key, value]) => ({
            ...sales,
            [key]: value.map(formatSale),
            _hash: +Date.now(),
          }), {}),
        }
      },
    },
  },
  initialState
)
