import { createAction } from 'redux-actions'
import identity from 'lodash/identity'
import { tsmDBMetaCreator } from 'utils/actions'

export const BUYS_LOADED = 'BUYS_LOADED'
export const buysLoaded = createAction(BUYS_LOADED, identity, tsmDBMetaCreator)
