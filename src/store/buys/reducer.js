import { handleActions } from 'redux-actions'
import { BUYS_LOADED } from './actions'

const initialState = {
  buys: {},
}

const formatBuy = buy => ({
  ...buy,
  time: parseInt(buy.time, 10) * 1000,
  price: parseInt(buy.price, 10),
  quantity: parseInt(buy.quantity, 10),
  stackSize: parseInt(buy.stackSize, 10),
})

export default handleActions(
  {
    [BUYS_LOADED]: {
      next(state, action) {
        return {
          ...state,
          buys: Object.entries(action.payload).reduce((buys, [key, value]) => ({
            ...buys,
            [key]: value.map(formatBuy),
            _hash: +Date.now(),
          }), {}),
        }
      },
    },
  },
  initialState
)
