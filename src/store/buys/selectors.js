export const selectBuys = (state, realm) => {
  if (!(realm in state.buys.buys)) {
    return {}
  }
  return state.buys.buys[realm]
}
