import { all, call, put, takeLatest } from 'redux-saga/effects'
// import moment from 'moment'
import { REALMS_LOADED } from 'store/realms/actions'
import { getBuys } from 'utils/accountingFile'
import { buysLoaded } from './actions'

function* loadBuysFlow(action) {
  const realms = action.payload
  const buys = realms.reduce((buys, realm) => ({
    ...buys,
    [realm]: getBuys(action.meta.tsmDB, realm),
  }), {})
  yield put(buysLoaded(buys, action.meta.tsmDB))
}

function* watchRealmsLoaded() {
  yield takeLatest(REALMS_LOADED, loadBuysFlow)
}

export default function* rootSaga() {
  yield all([
    call(watchRealmsLoaded),
  ])
}
