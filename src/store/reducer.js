// https://github.com/diegohaz/arc/wiki/Reducers
import { combineReducers } from 'redux'
import { reducer as form } from 'redux-form'
import parser from './parser/reducer'
import accountingFile from './accountingFile/reducer'
import buys from './buys/reducer'
import characters from './characters/reducer'
import realms from './realms/reducer'
import sales from './sales/reducer'
import user from './user/reducer'

const reducers = {
  accountingFile,
  buys,
  characters,
  parser,
  form,
  realms,
  sales,
  user,
}

export default combineReducers(reducers)
