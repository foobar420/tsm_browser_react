import { createAction } from 'redux-actions'
import identity from 'lodash/identity'
import { metaCreator } from 'utils/actions'

export const PARSER_CHANGE_FILE = 'PARSER_CHANGE_FILE'
export const PARSER_CHANGE_FILE_COMPLETE = 'PARSER_CHANGE_FILE_COMPLETE'
export const parserChangeFile = createAction(PARSER_CHANGE_FILE, identity, metaCreator)
export const parserChangeFileComplete = createAction(PARSER_CHANGE_FILE_COMPLETE)

export const PARSER_PARSE_TOKENS = 'PARSER_PARSE_TOKENS'
export const PARSER_PARSE_TOKENS_COMPLETE = 'PARSER_PARSE_TOKENS_COMPLETE'
export const parserParseTokens = createAction(PARSER_PARSE_TOKENS)
export const parserParseTokensComplete = createAction(PARSER_PARSE_TOKENS_COMPLETE)

export const PARSER_READ_FILE = 'PARSER_READ_FILE'
export const PARSER_READ_FILE_COMPLETE = 'PARSER_READ_FILE_COMPLETE'
export const parserReadFile = createAction(PARSER_READ_FILE)
export const parserReadFileComplete = createAction(PARSER_READ_FILE_COMPLETE)

export const PARSER_TOKENIZE_FILE = 'PARSER_TOKENIZE_FILE'
export const PARSER_TOKENIZE_FILE_COMPLETE = 'PARSER_TOKENIZE_FILE_COMPLETE'
export const parserTokenizeFile = createAction(PARSER_TOKENIZE_FILE)
export const parserTokenizeFileComplete = createAction(PARSER_TOKENIZE_FILE_COMPLETE)

export const PARSER_TOKENIZE_CHUNK_PARSED = 'PARSER_TOKENIZE_CHUNK_PARSED'
export const parserTokenizeChunkParsed = createAction(PARSER_TOKENIZE_CHUNK_PARSED)
