import { handleActions } from 'redux-actions'
import {
  PARSER_CHANGE_FILE,
  PARSER_CHANGE_FILE_COMPLETE,
  PARSER_PARSE_TOKENS,
  PARSER_PARSE_TOKENS_COMPLETE,
  PARSER_READ_FILE,
  PARSER_READ_FILE_COMPLETE,
  PARSER_TOKENIZE_FILE,
  PARSER_TOKENIZE_FILE_COMPLETE,
  PARSER_TOKENIZE_CHUNK_PARSED,
} from './actions'

const initialState = {
  changing: false,
  reading: false,
  tokenizing: false,
  parsing: false,
  loaded: false,
  error: null,
  exports: null,
  rawFileContent: null,
  fileSize: null,
  tokenize: {
    current: 0,
    max: 0,
    numTokens: 0,
  },
}

const reducer = handleActions(
  {
    [PARSER_READ_FILE]: {
      next(state) {
        return {
          ...state,
          reading: true,
        }
      },
    },
    [PARSER_READ_FILE_COMPLETE]: {
      next(state, action) {
        return {
          ...state,
          reading: false,
          rawFileContent: action.payload.content,
          fileSize: action.payload.content.length,
        }
      },
      throw(state, action) {
        return {
          ...state,
          reading: false,
          changing: false,
          error: action.payload,
        }
      },
    },
    [PARSER_TOKENIZE_FILE]: {
      next(state) {
        return {
          ...state,
          tokenizing: true,
        }
      },
    },
    [PARSER_TOKENIZE_FILE_COMPLETE]: {
      next(state) {
        return {
          ...state,
          tokenizing: false,
        }
      },
      throw(state, action) {
        return {
          ...state,
          tokenizing: false,
          changing: false,
          error: action.payload,
        }
      },
    },
    [PARSER_PARSE_TOKENS]: {
      next(state) {
        return {
          ...state,
          parsing: true,
        }
      },
    },
    [PARSER_TOKENIZE_CHUNK_PARSED]: {
      next(state, action) {
        return {
          ...state,
          tokenize: {
            ...state.tokenize,
            current: action.payload.current,
            max: action.payload.max,
          },
        }
      },
    },
    [PARSER_PARSE_TOKENS_COMPLETE]: {
      next(state, action) {
        return {
          ...state,
          parsing: false,
          exports: action.payload.exports,
        }
      },
      throw(state, action) {
        return {
          ...state,
          parsing: false,
          changing: false,
          error: action.payload,
        }
      },
    },
    [PARSER_CHANGE_FILE]: {
      next(state) {
        return {
          ...state,
          changing: true,
          exports: null,
          rawFileContent: null,
          error: null,
        }
      },
    },
    [PARSER_CHANGE_FILE_COMPLETE]: {
      next(state) {
        return {
          ...state,
          changing: false,
          loaded: true,
        }
      },
      throw(state, action) {
        return {
          ...state,
          changing: false,
          error: action.payload,
        }
      },
    },
  },
  initialState
)

export default reducer
