import { parse, tokenize } from 'parser'
import { takeLatest, call, all, apply, put } from 'redux-saga/effects'
import {
  PARSER_CHANGE_FILE,
  parserChangeFileComplete,
  parserParseTokens,
  parserParseTokensComplete,
  parserReadFile,
  parserReadFileComplete,
  parserTokenizeFile,
  parserTokenizeFileComplete,
  parserTokenizeChunkParsed,
} from './actions'

function idle(timeout = 0) {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout)
  })
}

function readFileAsText(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = ev => resolve(ev.target.result)
    reader.onerror = ev => reject(ev.target.error)
    reader.readAsText(file)
  })
}

function* parserChangeFileFlow(action) {
  const {
    payload: { file },
    meta: { resolve, reject },
  } = action
  let content
  let tokens = []
  yield put(parserReadFile(file))
  try {
    content = yield call(readFileAsText, file)
    yield put(parserReadFileComplete({ file, content }))
  } catch (e) {
    yield put(parserReadFileComplete(e))
    yield put(parserChangeFileComplete(e))
    yield call(reject, e)
    return
  }
  yield put(parserTokenizeFile({ content }))
  try {
    const chunkSize = 2097152 // 2mb, very artificial
    const idleTimeout = 333 // 333ms, very artificial too... this was to not interrupt running transitions
    const gen = tokenize(content, chunkSize)
    let res = yield apply(gen, gen.next)
    while (!res.done) {
      const {
        current,
        max,
      } = res.value
      yield put(parserTokenizeChunkParsed({
        current,
        max,
      }))
      yield call(idle, idleTimeout)
      res = yield apply(gen, gen.next)
    }
    if (res.done) tokens = res.value
    yield put(parserTokenizeFileComplete())
  } catch (e) {
    yield put(parserTokenizeFileComplete(e))
    yield put(parserChangeFileComplete(e))
    yield call(reject, e)
    return
  }
  yield put(parserParseTokens({ tokens }))
  try {
    const exports_ = yield call(parse, tokens)
    window.exports_ = exports_
    window.FI = function (k) {
      return window.exports_.TradeSkillMasterDB[k]
    }
    yield put(parserParseTokensComplete({ exports: exports_ }))
    yield put(parserChangeFileComplete())
    yield call(resolve)
  } catch (e) {
    yield put(parserParseTokensComplete(e))
    yield put(parserChangeFileComplete(e))
    yield call(reject, e)
  }
}

function* watchParserChangeFile() {
  yield takeLatest(PARSER_CHANGE_FILE, parserChangeFileFlow)
}

export default function* root() {
  yield all([
    call(watchParserChangeFile),
  ])
}
