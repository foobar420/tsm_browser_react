export const selectState = state => state.parser
export const selectIsChanging = state => state.parser.changing
export const selectIsReading = state => state.parser.reading
export const selectIsTokenizing = state => state.parser.tokenizing
export const selectIsParsing = state => state.parser.parsing
export const selectIsLoaded = state => state.parser.loaded
export const selectExports = state => state.parser.exports
export const selectRawFileContent = state => state.parser.rawFileContent
export const selectFileSize = state => state.parser.fileSize
export const selectError = state => state.parser.error

export const selectTokenize = state => state.parser.tokenize
export const selectTokenizeCurrentIndex = state => selectTokenize(state).current
export const selectTokenizeMaxIndex = state => selectTokenize(state).max
export const selectTokenizeTokens = state => selectTokenize(state).tokens
export const selectTokenizeNumTokens = state => selectTokenize(state).numTokens


// remove me when done debugging / inspecting

