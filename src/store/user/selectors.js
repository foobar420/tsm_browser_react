export const selectRealm = state => state.user.realm
export const selectDateFrom = state => state.user.dateFrom
export const selectDateTo = state => state.user.dateTo
