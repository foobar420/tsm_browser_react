import { handleActions } from 'redux-actions'
import {
  CHANGE_DATE_FROM,
  CHANGE_DATE_TO,
  CHANGE_REALM,
} from './actions'

const initialState = {
  realm: '',
  dateFrom: '2018-01-01',
  dateTo: '2018-01-01',
}

export default handleActions(
  {
    [CHANGE_REALM]: {
      next(state, action) {
        console.log('CHANGE_REALM', action.payload)
        return {
          ...state,
          realm: action.payload,
        }
      },
    },
    [CHANGE_DATE_FROM]: {
      next(state, action) {
        return {
          ...state,
          dateFrom: action.payload,
        }
      },
    },
    [CHANGE_DATE_TO]: {
      next(state, action) {
        return {
          ...state,
          dateTo: action.payload,
        }
      },
    },
  },
  initialState
)
