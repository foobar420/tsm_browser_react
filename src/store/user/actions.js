import { createAction } from 'redux-actions'

export const CHANGE_REALM = 'CHANGE_REALM'
export const changeRealm = createAction(CHANGE_REALM)

export const CHANGE_DATE_FROM = 'CHANGE_DATE_FROM'
export const changeDateFrom = createAction(CHANGE_DATE_FROM)

export const CHANGE_DATE_TO = 'CHANGE_DATE_TO'
export const changeDateTo = createAction(CHANGE_DATE_TO)

