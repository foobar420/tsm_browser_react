export function parseCsvString(csvStr) {
  const lines = csvStr.split('\\n')
  const headers = lines[0].split(',')
  return lines.slice(1).map( // eslint-disable-line
    line =>
      line
        .split(',')
        .reduce((obj, cell, i) => ({ ...obj, [headers[i]]: cell }), {})
  ) // eslint-disable-line
}
