export function offsetGet(arr, off, def) {
  return off < arr.length ? arr[off] : def
}
