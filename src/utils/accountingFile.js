import { parseCsvString } from './csv'
import { offsetGet } from './other'

export const KeyType = {
  Character: 'c',
  Faction: 'f',
  Global: 'g',
  Profile: 'p',
  Realm: 'r',
  Statistics: 's',
}

const OWNER_GLUE = ' - '
const KEY_GLUE = '@'

export function makeKey(type, owner, rootKey, subKey) {
  if (type.startsWith('_') && !owner && !rootKey && !subKey) return type
  return [type, owner, rootKey, subKey].join(KEY_GLUE)
}

function access(tsmDB, type, owner, rootKey, subKey)  {
  const key = makeKey(type, owner, rootKey, subKey)
  if (key in tsmDB) return tsmDB[key]
  return null
}

export function getSales(tsmDB, realm) {
  const csvStr = access(tsmDB, KeyType.Realm, realm, 'internalData', 'csvSales')
  return csvStr ? parseCsvString(csvStr) : null
}

export function getBuys(tsmDB, realm) {
  const csvStr = access(tsmDB, KeyType.Realm, realm, 'internalData', 'csvBuys')
  return csvStr ? parseCsvString(csvStr) : null
}

export function getExpenses(tsmDB, realm) {
  const csvStr = access(tsmDB, KeyType.Realm, realm, 'internalData', 'csvExpenses')
  return csvStr ? parseCsvString(csvStr) : null
}

const getOwnerOffset = (owner, offset) =>
  offsetGet(owner.split(OWNER_GLUE), offset)

export function getCharactersByRealm(tsmDB, realm) {
  return Object.values(tsmDB._scopeKeys.char)
    .filter(owner => getOwnerOffset(owner, 1) === realm)
    .map(owner => getOwnerOffset(owner, 0))
}

export function getCharacterClass(tsmDB, realm, faction, character) {
  const ownerKey = [character, faction, realm].join(OWNER_GLUE)
  return access(tsmDB, KeyType.Statistics, ownerKey, 'internalData', 'classKey')
}

export function getCharacterFaction(tsmDB, character, realm) {
  const owner = Object.values(tsmDB._scopeKeys.sync)
    .find(owner => owner.startsWith(character) && owner.endsWith(realm))
  return owner ? getOwnerOffset(owner, 1) : null
}

export function getRealms(tsmDB) {
  return Object.values(tsmDB._scopeKeys.realm)
}

export function getAllCharacters(tsmDB) {
  return Object.values(tsmDB._scopeKeys.sync)
    .map(owner => ({
      character: getOwnerOffset(owner, 0),
      faction: getOwnerOffset(owner, 1),
      realm: getOwnerOffset(owner, 2),
    }))
}

export function itemValue(price, quantity, stackSize) {
  return price * quantity * stackSize
}
