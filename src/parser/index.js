const nameRx = /[a-zA-Z0-9_]+/i
const whitespaceRx = /\s+/

const isName = s => s.match(nameRx)
const isWhitespace = s => s.match(whitespaceRx)

const TokenType = {
  NAME: 'name',
  STRING: 'string',
  NUMBER: 'number',
  SYMBOL: 'symbol',
}

class Token {
  constructor(tokenStr, type, line, column) {
    this.tokenStr = tokenStr
    this.type = type
    this.line = line
    this.column = column
  }

  val() {
    switch (this.type) {
      case TokenType.STRING:
        return this.tokenStr.slice(1, -1)
      case TokenType.NUMBER:
        return parseFloat(this.tokenStr)
      case TokenType.NAME:
      case TokenType.SYMBOL:
      default:
        return this.tokenStr
    }
  }
}

function assertBounds(tokenIndex, tokenList) {
  if (tokenIndex > tokenList.length - 1) {
    throw new Error(`Requested access to token out of bounds at index ${tokenIndex}. There are ${tokenList.length} tokens.`)
  }
}

function matchesType(tokenIndex, type, tokenList) {
  assertBounds(tokenIndex, tokenList)
  if (Array.isArray(type)) {
    return type.filter(ty => ty === tokenList[tokenIndex].type).length >= 1
  }
  return tokenList[tokenIndex].type === type
}

function matchesExact(tokenIndex, exact, tokenList) {
  assertBounds(tokenIndex, tokenList)
  if (Array.isArray(exact)) {
    return exact.filter(ex => ex === tokenList[tokenIndex].tokenStr).length >= 1
  }
  return tokenList[tokenIndex].tokenStr === exact
}

function matchesSymbol(tokenIndex, symbol, tokenList) {
  return matchesType(tokenIndex, TokenType.SYMBOL, tokenList) && matchesExact(tokenIndex, symbol, tokenList)
}

function assertMatchesType(tokenIndex, type, tokenList) {
  if (!matchesType(tokenIndex, type, tokenList)) {
    throw new Error(`Expected token at index=${tokenIndex},line=${tokenList[tokenIndex].line},column=${tokenList[tokenIndex].column} to be of type ${type}.
It is of type ${tokenList[tokenIndex].type}.`)
  }
}

function assertMatchesSymbol(tokenIndex, symbol, tokenList) {
  if (!matchesSymbol(tokenIndex, symbol, tokenList)) {
    throw new Error(`Expected token at index=${tokenIndex},line=${tokenList[tokenIndex].line},column=${tokenList[tokenIndex].column} to match symbol ${symbol} exactly.
It is ${tokenList[tokenIndex].tokenStr}.`)
  }
}


function readObjectKeyAt(tokenIndex, tokenList) {
  assertMatchesSymbol(tokenIndex, '[', tokenList)
  assertMatchesType(tokenIndex + 1, [TokenType.STRING, TokenType.NUMBER], tokenList)
  const key = tokenList[tokenIndex + 1].val()
  assertMatchesSymbol(tokenIndex + 2, ']', tokenList)
  return {
    key,
    endIndex: tokenIndex + 3,
  }
}

function readObjectValueAt(tokenIndex, tokenList) {
  if (matchesSymbol(tokenIndex, '{', tokenList)) {
    const obj = readObjectAt(tokenIndex + 1, tokenList) // eslint-disable-line no-use-before-define
    return {
      value: obj.obj,
      endIndex: obj.endIndex,
    }
  }
  return {
    value: tokenList[tokenIndex].val(),
    endIndex: tokenIndex + 1,
  }
}

function readObjectAt(tokenIndex, tokenList) {
  assertBounds(tokenIndex, tokenList)
  let i = tokenIndex
  let keyContext
  let valueContext
  const obj = {}
  let j = 0
  while (true) {
    if (matchesSymbol(i, '}', tokenList)) {
      return {
        obj,
        endIndex: i + 1,
      }
    }
    if (matchesSymbol(i, '[', tokenList)) {
      keyContext = readObjectKeyAt(i, tokenList)
      assertMatchesSymbol(keyContext.endIndex, '=', tokenList)
      valueContext = readObjectValueAt(keyContext.endIndex + 1, tokenList)
    } else {
      valueContext = readObjectValueAt(i, tokenList)
      keyContext = {
        key: j,
      }
      j += 1
    }
    if (matchesSymbol(valueContext.endIndex, '}', tokenList)) {
      i = valueContext.endIndex + 1
      break
    }
    obj[keyContext.key] = valueContext.value
    i = valueContext.endIndex + 1
  }
  return {
    obj,
    endIndex: i,
  }
}

function* tokenize(content, chunkSize) {
  const code = content.toString()
  const len = code.length
  const tokenList = []
  let i = 0
  let line = 0
  let column = 0
  let tokenBuffer = ''
  let tokenLine = 0
  let tokenColumn = 0
  let inSingleQuotes = false
  let inDoubleQuotes = false
  let inComment = false
  let lastCharWasBackspace = false
  const parseNextSym = (i) => {
    const sym = code[i]
    if (inSingleQuotes) {
      tokenBuffer += sym
      if (sym === '\'' && !lastCharWasBackspace) {
        tokenList.push(new Token(tokenBuffer, TokenType.STRING, tokenLine, tokenColumn))
        tokenBuffer = ''
        inSingleQuotes = false
      }
      lastCharWasBackspace = sym === '\\'
    } else if (inDoubleQuotes) {
      tokenBuffer += sym
      if (sym === '"' && !lastCharWasBackspace) {
        tokenList.push(new Token(tokenBuffer, TokenType.STRING, tokenLine, tokenColumn))
        tokenBuffer = ''
        inDoubleQuotes = false
      }
      lastCharWasBackspace = sym === '\\'
    } else if (inComment) {
      if (sym === '\n') {
        inComment = false
      }
    } else if (i + 1 < len && code[i] === '-' && code[i + 1] === '-') {
      inComment = true
    } else if (sym === '\'') {
      if (tokenBuffer.length) {
        const tokenType = tokenBuffer.match(/^-?[0-9.]+$/) ? TokenType.NUMBER : TokenType.NAME
        tokenList.push(new Token(tokenBuffer, tokenType, tokenLine, tokenColumn))
        tokenBuffer = ''
      }
      inSingleQuotes = true
      tokenBuffer = sym
      tokenLine = line
      tokenColumn = column
      lastCharWasBackspace = false
    } else if (sym === '"') {
      if (tokenBuffer.length) {
        const tokenType = tokenBuffer.match(/^-?[0-9.]+$/) ? TokenType.NUMBER : TokenType.NAME
        tokenList.push(new Token(tokenBuffer, tokenType, tokenLine, tokenColumn))
        tokenBuffer = ''
      }
      inDoubleQuotes = true
      tokenBuffer = sym
      tokenLine = line
      tokenColumn = column
      lastCharWasBackspace = false
    } else if (isWhitespace(sym)) {
      lastCharWasBackspace = false
    } else if (isName(sym)) {
      if (tokenBuffer.length) {
        tokenBuffer += sym
      } else {
        tokenBuffer = sym
        tokenLine = line
        tokenColumn = column
      }
      lastCharWasBackspace = false
    } else if (sym === '.' || sym === '-') {
      if (tokenBuffer.length) {
        tokenBuffer += sym
      } else {
        tokenBuffer = sym
        tokenLine = line
        tokenColumn = column
      }
      lastCharWasBackspace = false
    } else {
      if (tokenBuffer.length) {
        const tokenType = tokenBuffer.match(/^-?[0-9.]+$/) ? TokenType.NUMBER : TokenType.NAME
        tokenList.push(new Token(tokenBuffer, tokenType, tokenLine, tokenColumn))
        tokenBuffer = ''
      }
      tokenList.push(new Token(sym, TokenType.SYMBOL, line, column))
      lastCharWasBackspace = sym === '\\'
    }

    column += 1
    if (sym === '\n') {
      column = 0
      line += 1
    }
  }
  while (true) {
    if (i >= code.length) return tokenList
    const chunk = i + chunkSize
    for (; i < chunk && i < code.length; i++) parseNextSym(i)
    yield {
      current: i,
      max: code.length,
    }
  }
}

function parse(tokenList) {
  return new Promise((resolve, reject) => {
    try {
      assertMatchesType(0, TokenType.NAME, tokenList)
      assertMatchesSymbol(1, '=', tokenList)
      assertMatchesSymbol(2, '{', tokenList)
      const exportName = tokenList[0].val()
      const exportObj = readObjectAt(3, tokenList)
      resolve({
        [exportName]: exportObj.obj,
      })
    } catch (e) {
      reject(e)
    }
  })
}

export {
  parse,
  tokenize,
  Token,
  TokenType,
}
